## 安装
需要安装 [octave](https://www.gnu.org/software/octave/) 方能运行

## 使用
参见此贴：[rm能否使用调色板](http://rpg.blue/forum.php?mod=redirect&goto=findpost&ptid=409747&pid=2777526&fromuid=210672)

## 素材
图片素材仅供测试用，感谢 rpg.blue 论坛上的坛友 89444640 。

## 算法
假设已经给出了以下2张图片，原图和配色修改过后的参考图：

### 素材
原图|参考图|相似图
:-:|:-:|:-:
![red](red.png)|![green](green.png)|![red_04](images/red_04.png)

### 训练
1. 从原图![red](red.png)中学习聚类，将此图分成若干个区域 `C = {C1, C2, ... Cn}`，其聚类中心为 `c = {c1, c2, ... cn}`
2. 直接使用 `C` 代入参考图![green](green.png)做 1 步迭代，获得新的聚类中心 `c'`,
3. 在参考图上继续迭代，由 `c'` 获得新的聚类分类 `C'`
4. 比较 `C'` 和 `C` 的相似度，取不同的初始条件获得最大的相似度 `mean(C == C')`
5. 如此选出最优的训练集 `train = {c, C}`，其中的数据都是属于原图![red](red.png)

### 上色
对相似图![red_04](images/red_04.png)中的每一个不透明的像素做如下操作：

1. 比较聚类中心 `c` 和该像素的距离，选出最近的聚类中心 `cx`
2. 在对应的区域 `Cx` 中选出离该像素最近的点 `[x, y]`
    - `[x, y]` 在原图![red](red.png)上
3. 用参考图![green](green.png)上 `[x, y]` 处的像素替换相似图![red_04](images/red_04.png)上的该像素
