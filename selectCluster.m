function [c, C] = selectCluster(data, init_c)
  C = findCluster(data, init_c);
  for iter=1:1:100
    c = setCluster(data, C);
    old_C = C;
    C = findCluster(data, c);
    if old_C == C
      break;
    end
  end
end