function distance = colorDistance(data, color_vec)
  % alpha = [R, G, B, X, Y]
  global color_weight;
  r = data - color_vec;
  distance = sum(r .* r .* color_weight, 2);
end