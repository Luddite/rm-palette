%% main

clear;clc;
mkdir cluster;
mkdir result;
delete cluster/*.png;
delete result/*.png;
warning('off');
tic;
% recommend parameters %

% c_num = 2;
% global dis_weight = [1, 1, 1, 0.5, 0.5];
% iteration_max = 100;

c_num = 2;
global color_weight = [1, 1, 1, 0.5, 0.05];
iteration_max = 100;

file_before_convert = 'red.png';
file_after_convert = 'green.png';

% conver red 2 green
%% read picture
[img, map, alpha] = imread(file_after_convert);
[green, height, width] = img2data(img);

[img, map, alpha] = imread(file_before_convert);
[red, height, width] = img2data(img);

%% find cluster
data = red;
[m, dim] = size(data);

disp('start training...');

perp = 0;

for i=1:1:iteration_max
  init_c = data(randperm(m, c_num), :);
  [c, C] = selectCluster(data, init_c);

  c_green = setCluster(green, C);
  C_green = findCluster(green, c_green);
  if mean(C_green == C) > perp
    C_best = C;
    perp = mean(C_green == C);
    printf('best solution find at %d / %d, perp = %.4f\n', i, iteration_max, perp);fflush(1);
  end
end

C = C_best;
c_red = setCluster(red, C);
C_red = findCluster(red, c_red);

%% set red clusters
red_clusters = cell(c_num, 1);

for i=1:1:c_num
  tmp = (red .* (C == i));
  red_clusters(i) = tmp(find(any(tmp')' == 1), :);
end

%% -- end of find cluster

%% show & save cluster
printf('save clusters...\n');fflush(1);

for i=1:1:c_num
  green_sub = (green .* (C == i));
  img_sub = data2img(green_sub, height, width);
  fn = ['cluster/new_', num2str(i), '_', file_before_convert];
  imwrite(img_sub, fn, 'ALPHA', alpha);
end

for i=1:1:c_num
  red_sub = (red .* (C == i));
  img_sub = data2img(red_sub, height, width);
  fn = ['cluster/new_', num2str(i), '_', file_after_convert];
  imwrite(img_sub, fn, 'ALPHA', alpha);
end

printf('show new pictures...\n');fflush(1);

%% read red_01 for test
all_images = cell2mat(glob('images/*'));
for img_index=1:1:size(all_images, 1)
  fn_before = all_images(img_index, :);
  fn_after = sprintf('result/%s', fn_before(8:end));

  [img, map, alpha] = imread(fn_before);
  [new, height, width] = img2data(img);

  C_new = findCluster(new, c_red);

  for i=1:1:m
    h = new(i, 4) .* height;
    w = new(i, 5) .* width;
    if alpha(h, w) == 0
      continue;
    end
    sub = cell2mat(red_clusters(C_new(i)));
    distance = colorDistance(sub, new(i, :));
    [_, min_index] = min(distance);
    pos = [1 sub(min_index, 4:5)] * [-1, 1, width]' * height;
    new(i, :) = green(pos, :);
  end

  img = data2img(new, height, width);

  imwrite(img, fn_after, 'ALPHA', alpha);
  printf('%s done.\n', fn_before);
  fflush(1);
end

toc;