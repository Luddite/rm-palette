function C = findCluster(data, c)
  [m, dim] = size(data);
  number = size(c, 1);

  distance = zeros(m, number);

  for i=1:1:number
    distance(:, i) = colorDistance(data, c(i, :));
  end

  [_, C] = min(distance');
  C = C';
end