function c = setCluster(data, C)
  [m, dim] = size(data);
  number = max(C);
  
  c = zeros(number, dim);
  
  for i=1:1:number
    Ci = (C == i);
    c(i, :) = sum(data .* Ci) / (sum(Ci));
  end
end
