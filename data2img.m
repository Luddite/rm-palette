function img = data2img(data, height, width)
  img = reshape(data(:, 1:3), height, width, 3);
  % imshow(img);
end