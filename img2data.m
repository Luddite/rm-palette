function [imgData, height, width] = img2data(img)  
  [height, width, color] = size(img);
  % [R, G, B, X, Y]
  imgData = zeros(height * width, (color + 2));
  for h=1:1:height
    for w=1:1:width
      index = (w - 1) * height + h;
      imgData(index, :) = [img(h,w,1), img(h,w,2), img(h,w,3), h, w];
    end
  end
  imgData = double(imgData) ./ [256, 256, 256, height, width];
end